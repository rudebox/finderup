<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="wrap--fluid padding--both page__wrapper flex flex--wrap">

    <?php get_template_part('parts/sidebar'); ?>

    <article class="col-sm-8 single__item page__content is-sidebar" itemscope itemtype="http://schema.org/BlogPosting">

      
      <h2 class="single__title" itemprop="headline">
        <?php the_title(); ?>
      </h2>
      
      <div class="single__post" itemprop="articleBody">
        <?php the_content(); ?>
      </div>

    </article>

  </section>

  <?php get_template_part('parts/social'); ?>
  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>