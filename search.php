<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">

<?php get_template_part('parts/page', 'header'); ?>

  <section class="search padding--both">
    <div class="wrap--fluid hpad clearfix center">

      <h2>Søgeresultat for:</h2>
      <strong><p><?php echo esc_attr(get_search_query()); ?></p></strong>


      <?php if (have_posts()): ?>
        <?php while (have_posts()): the_post(); ?>

        <article class="search__posts">

          <header>
            <h2 class="search__title">
              <a href="<?php the_permalink(); ?>"
                 title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
              </a>
            </h2>
          </header>

          <div itemprop="description">
            <?php the_excerpt(); ?>
          </div>

        </article>

        <?php endwhile; else: ?>

          <p>Vi kunne desværre ikke finde nogen resultater der matchede dit søgeresultat.</p>

      <?php endif; ?>
    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>