<?php 
/**
* Description: Lionlab info-menu repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


if (have_rows('info_menu', 'options') ) :

?>

<section class="info-menu blue--bg margin--both">
	<div class="wrap--fluid hpad clearfix info-menu__container">
		<?php 
			while (have_rows('info_menu', 'options') ) : the_row(); 
				$icon = get_sub_field('menu_icon');
				$title = get_sub_field('menu_title');
				$link = get_sub_field('menu_link');
		?>

		<a href="<?php echo $link; ?>" class="col-sm-4 info-menu__item">
			<?php echo file_get_contents($icon['url']); ?>
			<h4><?php echo esc_html($title); ?></h4>
		</a>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>