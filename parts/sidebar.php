<?php 
	//counter
	$i=0;	
?>

<?php if (!is_single() ) : ?>
<aside id="sidebar" class="sidebar col-sm-4 is-open">

	<div class="sidebar--wrapper">
		<div class="sidebar__toggle is-open">
			<i class="fas fa-times"></i> 
		</div>
		
		<div class="sidebar__content">
			<h5 class="sidebar__pagetitle"><?php echo esc_html(the_title()); ?></h5>
			<ul class="sidebar__indent">
				 <?php while(the_flexible_field('layout')): 
				 	//retrieve flexible
				 	$i++;
				 ?>
				
				 <?php if(get_row_layout() == 'wysiwygs'): 
				 	//dynamic output of anchor tag from wysywig row layout
				 ?>

				<?php if (get_sub_field('header') ) : ?>
				<li class="sidebar__anchor">
				 	<a href="#wysiwygs-<?php echo $i; ?>"><?php echo esc_html(the_sub_field('header')); ?></a>
				</li>
				<?php endif; ?>

				<?php  endif; endwhile; ?>
			</ul>

			<h5 class="sidebar__related-content">Relateret indhold</h5>
			<ul>
				<?php
				// Set up the objects needed
				$my_wp_query = new WP_Query();
				$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));

				//retrieve proper page name
				$page_title = get_proper_title($id);

				// Get the page as an Object
				$page =  get_page_by_title($page_title);


				// Filter through all pages and find parent page children
				$page_children = get_page_children( $page->ID, $all_wp_pages );

	
				echo '<a class="sidebar__related-links is-active">' . $page_title . '</a>';

				foreach ($page_children as $child) : 
					$link = get_page_link($child);
					$title = get_the_title($child);
				?>

				<a class="sidebar__related-links" href="<?php echo esc_url($link); ?>"><?php echo esc_html($title); ?></a>

				<?php endforeach; ?>

					<?php 
						if (have_rows('sidebar_links') ) : while (have_rows('sidebar_links') ) : the_row();
						$link = get_sub_field('sidebar_link');
						$title = get_sub_field('sidebar_title'); 
					?>

						<a class="sidebar__related-links" href="<?php echo esc_url($link); ?>"><?php echo esc_html($title); ?></a>

				<?php endwhile; endif; ?> 

			</ul>
		</div>
	</div>
	
</aside>

<?php else : ?> 
<aside id="sidebar" class="sidebar col-sm-4 is-open">

	<div class="sidebar--wrapper">
		<div class="sidebar__toggle is-open">
			<i class="fas fa-times"></i> 
		</div>
		
		<div class="sidebar__content">
			
			<h5 class="sidebar__related-content sidebar__related-content--single">Flere nyheder</h5>
			<ul>
				<?php
									// Query Arguments
					$args = array(
						'order' => 'DESC'
					);

					// The Query
					$news_cat = new WP_Query( $args );

					// The Loop
					if  ( $news_cat->have_posts() ) : while ( $news_cat->have_posts() ) :
						$news_cat->the_post();
				?>

				<a class="sidebar__related-links" href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
				
				<?php wp_reset_postdata(); ?>
				<?php endwhile; endif; ?>

			</ul>
		</div>
	</div>
	
</aside>
<?php endif; ?>