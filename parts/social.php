<?php 
/**
* Description: Lionlab Facebook fedd field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$title = get_field('social_title', 'options');
$fb_link = get_field('social_fb', 'options');
$ig_link = get_field('social_ig', 'options');
?>

<section class="social gray--bg padding--both">
	<div class="wrap--fluid hpad clearfix">
		<h2 class="social__title center"><?php echo esc_html($title); ?></h2>
		<div class="social__wrap center">
			<a target="_blank" href="<?php echo esc_url($ig_link); ?>" class="social__link social__link--ig"><i class="fab fa-instagram"></i> @finderupefterskole</a>
			
			<a target="_blank" class="social__link" href="<?php echo esc_url($fb_link); ?>"><i class="fab fa-facebook-square"></i> @Finderupefterskole</a>
		</div>
	</div>
	
	<!-- Facebook feed -->
	<div id="curator-feed"></div>
</section>