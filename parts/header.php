<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
    <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MC9472P');</script>
</head>


<body <?php body_class(); ?>>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MC9472P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


<?php 
    $rows = get_field('preloader', 'options');

    if (have_rows('preloader', 'options') ) : 

    shuffle($rows);  
    //counter
    $i=0;
?>

<div class="preloader__wrapper">
    <div class="preloader">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.png" alt="preloader">
    </div>
    <div class="preloader preloader--animation">

      <?php 
          foreach ($rows as $row) :

          $img = $row['img'];

          $i++;
       ?>
      
  
      <div class="preloader__img preloader__img--<?php echo esc_html($i); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
        
      </div>

      <?php endforeach; ?>

    </div>
</div> 
<?php endif; ?> 

<header class="header" id="header">
  <div class="wrap--fluid lpad flex flex--center flex--justify">

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.png" alt="<?php bloginfo('name'); ?>">
    </a>

    
    <nav class="nav" role="navigation">
        <div class="nav--mobile">
          <?php scratch_main_nav(); ?>
          <?php scratch_second_nav(); ?>
          <?php scratch_third_nav(); ?>
          <?php scratch_fourth_nav(); ?>
          <?php scratch_fifth_nav(); ?>
          <?php scratch_sixth_nav(); ?>
          <?php scratch_seventh_nav(); ?>
          <?php scratch_eight_nav(); ?>
        </div>
    </nav>

  </div>
</header>

<div class="toolbar">
  <div class="nav-toggle">  
    <span class="nav-toggle__icon"></span>
    <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span>
  </div>

  <form class="hidden-sm" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
        <div class="toolbar__form">
          <input type="text" value="<?php the_search_query(); ?>" placeholder="Indtast søgeord..." name="s" id="s"></input>
          <button type="submit" class="toolbar__search"><?php echo file_get_contents('wp-content/themes/finderup/assets/img/search.svg'); ?> Søg </button>
        </div>
  </form>

  <a href="http://www.adgangforalle.dk/" target="_blank" class="toolbar__link hidden-sm">
    <?php echo file_get_contents('wp-content/themes/finderup/assets/img/book.svg'); ?>
    Oplæs
  </a>

  <a href="/tilmelding" class="toolbar__link hidden-sm">
    <?php echo file_get_contents('wp-content/themes/finderup/assets/img/person.svg'); ?>
    Tilmeld
  </a>
  

  <a href="/" class="toolbar__link hidden-sm">
    <?php echo file_get_contents('wp-content/themes/finderup/assets/img/home.svg'); ?>
    Forside
  </a>
</div>