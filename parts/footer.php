<footer class="footer gray--bg" id="footer">
	<div class="wrap--fluid hpad clearfix footer__container">
		
		<?php 
			if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
				the_row();
			$title = get_sub_field('column_title');
			$text = get_sub_field('column_text');
		 ?>

		 <div class="col-sm-4 footer__item footer__item--<?php echo get_row_index(); ?>">
		 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
			<?php echo $text; ?>
		 </div>

		<?php endwhile; endif; ?>
		
	</div>
	<a class="footer__portal" href="#header"><?php echo file_get_contents('wp-content/themes/finderup/assets/img/btn.svg'); ?></a>
</footer>


<script type="text/javascript">
/* curator-feed */
(function(){
var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
i.src = "https://cdn.curator.io/published/c56b5367-f2d2-4b72-a450-71242d05de28.js";
e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);
})();
</script>

<?php wp_footer(); ?>

</body>
</html>
