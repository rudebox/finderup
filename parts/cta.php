<?php 
/**
* Description: Lionlab CTA field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$img = get_field('cta_bg', 'options');
$title = get_field('cta_title', 'options');
$link = get_field('cta_link', 'options');
$link_text = get_field('cta_link_text', 'options');
?>

<section class="cta" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="wrap--fluid hpad clearfix">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
				<h2 class="cta__title center"><?php echo esc_html($title); ?></h2>
				<a class="btn btn--hollow cta__btn" href="<?php echo esc_url($link); ?>"><span><?php echo esc_html($link_text); ?></span></a>
			</div>
		</div>
	</div>
</section>