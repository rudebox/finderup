<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$gallery = get_sub_field('gallery');

if ( $gallery ) : ?>

	<section class="gallery" itemscope itemtype="http://schema.org/ImageGallery">
		<div class="wrap--fluid hpad">
			<div class="flex flex--wrap gallery__track clearfix">

				<?php
				// Loop through gallery
				foreach ( $gallery as $image ) : ?>

					<div class="gallery__item">
						<a href="<?= $image['sizes']['large']; ?>" class="gallery__link" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">
							<img class="gallery__image" data-src="<?= $image['sizes']['gallery']; ?>" src="<?= $image['sizes']['gallery']; ?>" alt="<?= $image['alt']; ?>" itemprop="thumbnail" height="<?= $image['sizes']['large-height']; ?>" width="<?= $image['sizes']['large-width']; ?>">
						</a>
					</div>

				<?php endforeach; ?>

			</div>
		</div>
	</section>

<?php endif; ?>
