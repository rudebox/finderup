<?php
 /**
   * Description: Lionlab employees repeater field group
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

 if (have_rows('content-box') ) :
?>

<section class="content-box">
	<div class="wrap--fluid hpad">
		<div class="row flex flex--wrap clearfix">
		<?php while (have_rows('content-box') ) : the_row(); 
			$title = get_sub_field('header');
			$text = get_sub_field('text');
			$bg = get_sub_field('bg');
			$icon = get_sub_field('icon');
		?>

		<div class="col-md-4 content-box__item <?php echo esc_html($bg); ?>--bg">
			<div class="content-box__wrap">
				<?php echo $icon; ?>
				<h4><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			</div>
		</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>