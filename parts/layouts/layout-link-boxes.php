<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings

if (have_rows('linkbox') ) :

//counter
$i=0;
?>

<section class="link-boxes padding--both">
	<div class="wrap--fluid hpad">
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				
				$link = get_sub_field('link');

				//bg color
				$bg_color = get_sub_field('bg');

				//counter
				$i++;

				if ($i === 2 || $i === 4 || $i === 6) :
					$class = 'link-boxes__item--reverse';
				else:
					$class = '';
				endif;
			?>

			

			<div class="link-boxes__item <?php echo $class; ?>">
			
				<div class="col-sm-6 col-md-4 <?php echo $bg_color; ?>--bg link-boxes__text">
					<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
					<?php echo $text; ?>
					<a class="btn btn--white link-boxes__btn" href="<?php echo $link; ?>"><span>Læs mere</span></a>
				</div>

				<div data-0="top:-40px;" data-top="top:-10px;" class="link-boxes__wrap">
				<?php if (have_rows('imgs') ) : while (have_rows('imgs') ) : the_row(); $img = get_sub_field('img'); ?>			
					<div class="col-sm-6 col-md-4 link-boxes__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
				
					</div>				
				<?php endwhile; endif; ?>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>