<?php
 /**
   * Description: Lionlab employees repeater field group
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */
 
 if (have_rows('employee') ) :
?>

<section class="employees">
	<div class="wrap--fluid hpad">
		<div class="row clearfix flex flex--wrap">
		<?php while (have_rows('employee') ) : the_row(); 
			$img = get_sub_field('img');
			$name = get_sub_field('name');
			$title = get_sub_field('title');
			$mail = get_sub_field('mail');
			$phone = get_sub_field('phone');
		?>

		<div class="col-sm-6 col-lg-4 employees__item">
			<div class="employees__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>
			<div class="employees__content gray--bg">
				<staong class="employees__name"><?php echo esc_html($name); ?></staong>
				<span class="employees__title"><?php echo esc_html($title); ?></span>
				
				<?php if ($mail) : ?>
				<div class="employees__contact">
					<a class="employees__mail" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
				</div>
				<?php endif; ?>
				<?php if ($phone) : ?>
				<div class="employees__contact">
					<a class="employees__phone" href="tel:<?php echo esc_html($phone); ?>"><?php echo esc_html($phone); ?></a>
				</div>
				<?php endif; ?>
				
			</div>
		</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>