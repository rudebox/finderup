<?php 
/**
* Description: Lionlab testimonials repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings

if (have_rows('testimonial') ) :
?>

<section class="testimonials padding--bottom">
	<div class="wrap--fluid hpad">
		<div class="row flex flex--wrap">
		
			<?php while (have_rows('testimonial') ) : the_row(); 
				$name = get_sub_field('name');
				$text = get_sub_field('text');
				$img = get_sub_field('img');
				$link = get_sub_field('link');

				//bg color
				$bg_color = get_sub_field('bg');

				$index = get_row_index();

				//trim words
				$trim_text = get_sub_field('text');
				$words = 15;
				$more = '&hellip;';
			?>


			<div class="testimonials__wrap col-md-6">

				<div class="col-sm-4 testimonials__img testimonials__img--<?php echo $index; ?>">
					<h2>Udtalelse</h2>
					<img src="<?php echo esc_url($img['sizes']['thumbnail']); ?>" alt="testimonial_person">
				</div>

				<div class="testimonials__text col-sm-8 <?php echo esc_html($bg_color); ?>--bg">
					<strong class="testimonials__name"><?php echo esc_html($name); ?></strong>
					<em>
						<?php echo wp_trim_words($trim_text, $words, $more); ?><br>
						<?php if ($link) : ?>
						<a class="testimonials__btn" href="<?php echo esc_html($link); ?>">Læs mere</a>
						<?php endif; ?>
					</em>
					<i class="fas fa-quote-right"></i>
				</div>
			</div>
		
			<?php endwhile; ?>

		</div>
	</div>
</section>
<?php endif; ?>