<?php 
/**
* Description: Lionlab courses repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


if (have_rows('course') ) :

//counter
$i=0;
?>

<section class="courses padding--both">
	<div class="wrap--fluid hpad clearfix courses__container">
		<?php while (have_rows('course') ) : the_row(); 
			$course = get_sub_field('name');
			$img = get_sub_field('img');
			$link = get_sub_field('link');
			$bg = get_sub_field('bg');

			$i++;

			if ($i === 2 || $i === 5 || $i === 8 || $i === 11) :
				$style = 'data-0="top:0px;" data-top="top:80px;"';

			else: 
				$style = '';

			endif;
		?>
 
		<a href="<?php echo esc_url($link); ?>" <?php echo $style; ?> class="col-sm-4 courses__item courses__item--<?php echo $bg; ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
			<div class="courses__content">
				<h4 class="courses__title"><?php echo esc_html($course); ?></h4>
			</div>
		</a>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>