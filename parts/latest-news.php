<?php 
/**
* Description: Retrieve latests post from category news 
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

?>

<?php 
	// Query Arguments
	$args = array(
		'posts_per_page' => 1,
		'order' => 'DESC',
		'category_name' => 'nyheder',
	);

	// The Query
	$news = new WP_Query( $args );

	// The Loop
	if ( $news->have_posts() ) : while ( $news->have_posts() ) : $news->the_post();

?>

	<a href="<?php echo the_permalink(); ?>" class="col-lg-5 col-md-6 col-sm-7 news__post">
		<span><?php the_time('Y/m/d'); ?></span><strong><?php the_title(); ?></strong> <?php echo file_get_contents ('wp-content/themes/finderup/assets/img/btn.svg'); ?>
	</a>

<?php 
	endwhile; endif;
	/* Restore original Post Data */
	wp_reset_postdata();
 ?>



