<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png"> -->
  <!-- Place favicon.ico in the root directory -->

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css', false, null );

  wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css', false, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), null, true );
  }

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'CTA',
    'menu_title'  => 'CTA',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Facebook feed',
    'menu_title'  => 'Facebook feed',
    'parent_slug' => 'global-content',
  ));
}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' ),   // main nav in header
    'scratch-second-nav' => __( 'Second Nav', 'scratch' ),   // second nav in header
    'scratch-third-nav' => __( 'Third Nav', 'scratch' ),   // third nav in header
    'scratch-fourth-nav' => __( 'Fourth Nav', 'scratch' ),   // fourth nav in header
    'scratch-fifth-nav' => __( 'Fifth Nav', 'scratch' ),   // fifth nav in header
    'scratch-sixth-nav' => __( 'Sixth Nav', 'scratch' ),   // fifth nav in header
    'scratch-seventh-nav' => __( 'Seventh Nav', 'scratch' ),   // fifth nav in header
    'scratch-eigth-nav' => __( 'Eigth Nav', 'scratch' ),   // fifth nav in header
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */

function scratch_second_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => 'wrap', // class of container (should you choose to use it)
    'menu' => __( 'Second Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-second-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} 

function scratch_third_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Third Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-third-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
}  


function scratch_fourth_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Fourth Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-fourth-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
}

function scratch_fifth_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Fifth Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-fifth-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
}

function scratch_sixth_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Sixth Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-sixth-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
}

function scratch_seventh_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Seventh Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-seventh-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
}

function scratch_eight_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Eigth Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-eigth-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
}          

//append image to nav items
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
  
  // loop
  foreach( $items as &$item ) {
    
    // vars
    $img = get_field('menu_img', $item);
    
    
    // append img
    if( $img ) {
      
      $item->title .= '<div class="nav__featured-img" style="background-image: url(' . $img['url'] . ')";></div>';
      
    }
    
  }
  
  
  // return
  return $items;
  
}

// Image sizes
add_image_size('employee', 329, 544, true);
add_image_size('gallery', 500, 350, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array(
    'gallery' => __( 'Galleri' ),
    'employee' => __( 'Medarbejder' ),
  ) );
}  

//make gf submit input to button
function gf_make_submit_input_into_a_button_element($button_input, $form) {

  //save attribute string to $button_match[1]
  preg_match("/<input([^\/>]*)(\s\/)*>/", $button_input, $button_match);

  //remove value attribute
  $button_atts = str_replace("value='".$form['button']['text']."' ", "", $button_match[1]);

  return '<button '.$button_atts.'><span>'.$form['button']['text'].'</span></button>';
}
add_filter('gform_submit_button', 'gf_make_submit_input_into_a_button_element', 10, 2);



//gravity form scripts in footer
add_filter("gform_init_scripts_footer", "init_scripts");
  function init_scripts() {
  return true;
}

//gravity form styles in head
add_action('wp_enqueue_scripts', function() {
    if (function_exists('gravity_form_enqueue_scripts')) {
        // newsletter subscription form
        gravity_form_enqueue_scripts(5);
    }
});


add_filter( 'http_request_host_is_external', '__return_true' );


/* Don't delete this closing tag. */
?>
