<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="wrap--fluid hpad flex flex--wrap padding--both home__container">

  <?php if (have_posts()): 
      //counter
      $i=0;
    ?>
    
    <?php while (have_posts()): the_post(); 
      $i++;

      if ($i === 2 || $i === 5 || $i === 8 || $i === 11) :
        $style = 'data-0="top:0px;" data-top="top:80px;"';
        $class = 'green--bg';

      elseif ($i === 1 || $i === 4 || $i === 7 || $i === 10) :
        $class = 'blue--bg';
        $style = '';

      elseif ($i === 3 || $i === 6 || $i === 9 || $i === 12) : 
        $class = 'red--bg';
        $style = '';

      endif;
    ?>



    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>        

    <a href="<?php the_permalink(); ?>" <?php echo $style; ?> class="col-sm-4 home__post <?php echo $class; ?>" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url(<?php echo esc_url($thumb['0']); ?>);">
      
      <div class="home__overlay"></div>

      <div class="home__content">
        <h2 class="home__title" itemprop="headline">          
            <?php the_title(); ?>
        </h2>

        <?php the_excerpt(); ?>
      </div>

    </a>

    <?php endwhile; else: ?>

      <p>No posts here.</p>

  <?php endif; ?>

  </section>

  <?php get_template_part('parts/social'); ?>
  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>