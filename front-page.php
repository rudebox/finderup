<?php get_template_part('parts/header'); ?>

<main>
	
  <?php get_template_part('parts/content', 'layouts'); ?>
  <?php /* get_template_part('parts/info', 'menu'); */ ?> 
  <?php get_template_part('parts/social'); ?>
  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
