jQuery(document).ready(function($) {
  
  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
    $('.nav__menu').toggleClass('is-visible');
  });

  // if ($(window).width() < 768) {
  //   $('.nav__link-icon').click(function(e) {
  //     e.preventDefault();
  //     $('.nav__dropdown').addClass('is-visible');
  //   }); 

    
  //   $('.is-dropdown').on('click', function(){
      
  //     if($(this).hasClass('is-visible')) {
  //       $(this).removeClass('is-visible');
  //       $(this).children('.nav__dropdown').slideUp('fast');
  //     } else {
  //       $(this).addClass('is-visible');
  //       $(this).children('.nav__dropdown').slideDown('fast');
  //     }
  //   });
  // }


  // Hide toolbar on on scroll down
  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var toolbarWidth = $('toolbar').outerWidth(); 

  $(window).scroll(function(event){
      didScroll = true;
  });

  setInterval(function() {
      if (didScroll) {
          hasScrolled();
          didScroll = false;
      }
  }, 250);

  function hasScrolled() {
      var st = $(this).scrollTop();
      
      // Make sure they scroll more than delta
      if(Math.abs(lastScrollTop - st) <= delta)
          return;
      
      if (st > lastScrollTop && st > toolbarWidth){
          // Scroll Down
          $('.toolbar').removeClass('is-visible').addClass('is-hidden');
      } else {
          // Scroll Up
          if(st + $(window).height() < $(document).height()) {
              $('.toolbar').removeClass('is-hidden').addClass('is-visible');
          }
      }
      
      lastScrollTop = st;
  }


  //owl slider/carousel
  var owl = $('.slider__track');

  owl.each(function() {
    $(this).children().length > 1;

      $(this).owlCarousel({
        loop: true,
        items: 1,
        autoplay: true,
        // nav: true,
        dots: true,
        autplaySpeed: 11000, 
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200 
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });
  });
  


  $('a[href*="#"]').not('[href="#"]').on('click',function(e) {
    e.preventDefault();

    var target = this.hash;
    var $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });


  //toggle sidebar
  if ($(window).width() > 768) {   
    $('.sidebar__toggle').on('click', function() {
        $('.sidebar__content').slideToggle();
        $('.sidebar__toggle').toggleClass('is-open');
        $('.page__content').toggleClass('col-sm-8 col-sm-12 is-sidebar');
        $('.page__wrapper').toggleClass('wrap');
        $('.sidebar').toggleClass('is-open'); 
    });
  }

  
  if ($(window).width() < 768) {
    $('.sidebar__toggle').removeClass('is-open');

    $('.sidebar__toggle').on('click', function() {
      $('.sidebar__content').slideToggle();
      $('.sidebar__toggle').toggleClass('is-open'); 
  });
  }


   //ative class pagelink
  $('.sidebar__related-links').each(function() {   
      if (this.href == window.location.href) {
          $(this).addClass('is-active');
      }
  });

  if($('body').hasClass('is-fixed')) {
    $('toolbar').addClass('menu-open');
  }


   //fixed sidebar
   function stickyContent() {

    var wrapper = $('.sidebar'),
       wrapperHeight = wrapper.outerHeight(),
       wrapperPos = wrapper.offset().top,
       wrapperBottom = wrapperPos + wrapperHeight,
       content = $('.sidebar--wrapper'),
       contentHeight = content.outerHeight(),
       contentPos = content.offset().top + contentHeight,
       contentStop = $(window).scrollTop() + contentHeight,
       contentWidth = content.width();

    // When window reaches top of content wrapper
    if ( $(window).scrollTop() >= wrapperPos ) {
      content.addClass('is-sticky');
      content.css({
        width: contentWidth + 'px',
      })
    } else {
      content.removeClass('is-sticky');
      content.css({
        width: 'auto',
      })
    }

    // When content hits bottom of wrapper
    if ( contentStop >= wrapperBottom ) {
      content.removeClass('is-sticky');
      content.addClass('is-fixed');
    } else {
      content.removeClass('is-fixed');
    }
  }

  $(window).on('load resize scroll', function() {
    const MOBILE_BP = 768;
    var wW = $(window).width();

    if ( $('.sidebar--wrapper').length && wW > MOBILE_BP ) {
      stickyContent(); 
    }
  });


  //responsive table
  $('.wysiwygs__col table').wrap('<div class="table">');

  //remove l sep characters
  $(".wysiwygs__col").children().each(function() {
    $(this).html($(this).html().replace(/&#8232;/g," "));
  });


  //preloader 
  var Body = $('body');
  var Animation = $('.preloader--animation');
    Body.addClass('preloader-site is-fixed');
    Animation.addClass('preloader is-active');
    
    $(window).load(function(){

      setTimeout(function() { 
        $('.preloader__wrapper').fadeOut();
        $('body').removeClass('preloader-site is-fixed');
        Animation.removeClass('is-active');
        $('.slider__text').addClass('reveal-text');
        $('.page__title').addClass('reveal-text');
          }, 1000);

    }); 

  
  skrollr.init({forceHeight: false});

  var _skrollr = skrollr.get(); // get() returns the skrollr instance or undefined
  var windowWidth = $(window).width();

  if ( windowWidth <= 768 && _skrollr !== undefined ) {
    _skrollr.destroy(); 
  } 

  if ($('body').hasClass('is-iphone') ) {
      _skrollr.destroy();   
  }


  //safari link w. pseudo element double tap issue fix
  // $('a').on('click touchend', function(e) {
  //   var el = $(this);
  //   var link = el.attr('href');
  //   window.location = link;
  // });

});