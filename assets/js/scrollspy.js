jQuery(document).ready(function($) {
	//scrollspy

	function scrollSpy() {
		var $list = $('.sidebar__indent');

		$list.each(function() {
			if ( $(this).length ) {
				var $links = $(this).find('.sidebar__anchor a');

				$links.each(function() {
					var link = $(this);
					var scrollPos = $(window).scrollTop();
					var hash = this.href.indexOf('#');

					if ( link.length && hash !== -1 ) {
						var refElement = $(link.attr('href'));

						if ( isInViewport(refElement[0]) ) {
							link.parent().addClass('is-active');
						} else {
							link.parent().removeClass('is-active');
						}
					}
				});
			}
		});
	}

	

	$(document).on('scroll load resize', scrollSpy);


	
	//detect if element is in viewport	
	function isInViewport(elem) {
		var bounding = elem.getBoundingClientRect();
		return (
			bounding.top >= 0 &&
			bounding.left >= 0 &&
			bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
			bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
		);
	};


});