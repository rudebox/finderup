<?php

get_template_part('parts/header'); the_post(); ?>

<main class="clearfix">
  
  <?php get_template_part('parts/page', 'header');?>

  <?php 
    $show = get_field('show_sidebar'); 
    if ($show == true) {
      echo '<div class="flex flex--wrap padding--both page__wrapper">';
        get_template_part('parts/sidebar');

        echo '<div class="col-sm-8 page__content is-sidebar">';    
            get_template_part('parts/content', 'layouts');
        echo '</div>';
      echo '</div>';
    }

    else {
      // echo '<div class="wrap flex flex--wrap padding--both">';
        get_template_part('parts/content', 'layouts');
      // echo '</div>'; 
    }
  ?>

  <?php get_template_part('parts/social'); ?>
  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
