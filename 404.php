<?php get_template_part('parts/header'); ?>

<main>
  <?php get_template_part('parts/page', 'header');?>

  <section class="wrap--fluid lpad clearfix padding--both">
	
	<h2>Vi beklager</h2>
    <p>Vi kunne desværre ikke finde den side du søgte efter.</p>
	<a class="btn btn--gray" href="/"><span>Gå til forsiden</span></a>
  </section>

  <?php get_template_part('parts/social'); ?>
  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>